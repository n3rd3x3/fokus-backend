const bodyparser = require("body-parser")
const mongoose = require("mongoose")
const express = require("express")
const fs = require("fs")
const app = express()

const logging = require(`${__dirname}/structs/logs`)
const config = require(`${__dirname}/config.json`)

app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended: true}))

mongoose.connect(config.db, { useNewUrlParser: true, useUnifiedTopology: true}, async e => {
    if (e) throw e
    logging.fokus(`MongoDB connected and hopefully working`)
})

app.use(require(`${__dirname}/routes`))

global.backendversion = "0.2.0"
const listeningport = process.env.port || config.port || 3003;

app.listen(listeningport, () => {
    logging.fokus(`Version \x1b[36m${backendversion}`)
    logging.fokus(`Listening on port \x1b[36m${listeningport}`)
    logging.fokus("Hopefully everything is working now")
})