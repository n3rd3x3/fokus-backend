module.exports = {
    fokus: data => console.log(`[\x1b[35mFokus\x1b[0m] ${data}\x1b[0m`),
    error: data => console.error(`\x1b[31m[ERROR] ${data}\x1b[0m`),
    accounts: data => console.log(`[\x1b[33mAccounts\x1b[0m] ${data}\x1b[0m`),
}