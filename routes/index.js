const express = require("express")
const crypto = require("crypto")
const path = require("path")
const app = express.Router()

app.use("/", require(`${__dirname}/id.js`))

module.exports = app
