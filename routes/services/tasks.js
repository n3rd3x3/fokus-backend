const app = express.Router()
const config = require(`${__dirname}/../../config.json`)
const request = require("request")
const express = require("express")
const bcrypt = require("bcrypt")

const Tasks = require(`${__dirname}/../../model/tasks`)

app.all("/api/public/:tasks", checkToken, async (req, res) => {
    if (req.method != "GET") return res.status(405).json(errors.method())

    let user = await User.findOne({id: req.cookies["FOKUS_ID"]}).lean()
    if (tokens[req.cookies["FOKUS_ID"]] == req.cookies["FOKUS_TOKEN"]) {

        if (task) res.json({
            tasks: Tasks.tasks
        })
    }
    else return res.status(404).json(errors.create(
        "fokus.error.account_or_tasks_not_found", 18007,
        `Sorry, we couldn't find an account for ${req.cookies["FOKUS_ID"]}, or your tasks could not be found`
    ))
})

module.exports = app