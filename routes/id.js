const cookieParser = require("cookie-parser")
const request = require("request")
const express = require("express")
const bcrypt = require("bcrypt")
const crypto = require("crypto")
const uuid = require("uuid").v4
const app = express.Router()
const fs = require("fs")


const logging = require(`${__dirname}/../structs/logs`)
const User = require(`${__dirname}/../model/User`)
const Tasks = require(`${__dirname}/../model/Tasks`)
const Calender = require(`${__dirname}/../model/Calender`)
const Subjects = require(`${__dirname}/../model/Subjects`)

var tokens = {}

app.use(cookieParser())

function cappy(gcaptcha) {
    return new Promise((resolve, reject) => {
        request.post("https://www.google.com/recaptcha/api/siteverify", {json: true, form: {
                secret: "",
                response: gcaptcha
            }}, (err, res, body) => {
            try {
                if (err) resolve(false)
                resolve(body.success)
            } catch {resolve(false)}
        })
    })
}

app.post("/api/register", async (req, res) => {
    res.clearCookie("FOKUS_ID")
    res.clearCookie("FOKUS_TOKEN")

    var success = req.body.email && req.body.password

    if (!success) return res.status(400).json({
        error: `Missing '${[req.body.email ? null : "email", req.body.password ? null : "password",
            //req.body.captcha ? null : "captcha"
        ].filter(x => x != null).join(", ")}' field(s).`,
        errorCode: "fokus.id.register.invalid_fields",
        statusCode: 400
    })

    /* var failure = await cappy(req.body.captcha)
    if (!failure) return res.status(400).json({
        error: "Recaptcha response is invalid.",
        errorCode: "fokus.id.register.invalid_captcha",
        statusCode: 400
    }) */

    if (req.body.email.length > 50) return res.status(400).json({
        error: `Field email is too big.`,
        errorCode: `fokus.id.register.email_too_big`,
        statusCode: 400
    })
    
    if (typeof req.body.email !== 'string') return res.status(400).json({
        error: `Field email is not a string.`,
        errorCode: `fokus.id.register.email_not_string`,
        statusCode: 400
    })
    
    var checkemail = await User.findOne({email: new RegExp(`^${req.body.email}$`, 'i')}).lean().catch(e => next(e))
    if (checkemail != null) return res.status(400).json({
        error: `"Email" ${req.body.email}' already exists`,
        errorCode: `fokus.id.register.email_already_exists`,
        statusCode: 400
    })
    var useruuid = uuid()
    var user = new User({
        userid: useruuid,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10))
    })
    user.save()
    var tasks = new Tasks({
        userid: useruuid,
        tasks: []
    })
    tasks.save()
    var calender = new Calender({
        userid: useruuid,
    })
    calender.save()
    var subjects = new Subjects({
        userid: useruuid,
        subjects: []
    })
    logging.accounts(`Created account with the ID \x1b[36m${useruuid}`)

    res.redirect("/login")
})

app.post("/api/login", async (req, res) => {
    var validUser = req.body.email && req.body.password

    if (!validUser) return res.status(400).json({
        error: `Missing '${[req.body.email ? null : "email", req.body.password ? null : "password"].filter(x => x != null).join(", ")}`,
        errorCode: "fokus.id.login.invalid_fields",
        statusCode: 400
    })
    
    if (typeof req.body.email !== 'string') return res.status(400).json({
        error: `Field email is not a string.`,
        errorCode: `fokus.id.register.email_not_string`,
        statusCode: 400
    })

        var check = await User.findOne({email: new RegExp(`^${req.body.email}$`, 'i')})

        if (check) {
            if (bcrypt.compareSync(req.body.password, check.password)) {
                var token = crypto.randomBytes(16).toString("hex")
                tokens[check.userid] = token

                res.cookie("FOKUS_ID", check.userid)
                res.cookie("FOKUS_TOKEN", token)

                if (req.query.redirect) res.redirect("/login")
                else res.json({
                    access_token: token,
                    account_id: check.userid,
                    statusCode: 200
                })
            } else return res.status(401).json({
                error: `Incorrect password for the account '${req.body.email}'.`,
                errorCode: "fokus.id.login.password_incorrect",
                statusCode: 401
            })
        } else return res.status(404).json({
            error: `Account under the email '${req.body.email} not found.`,
            errorCode: "fokus.id.login.account_not_found",
            statusCode: 404
        })

    })


app.get("/api/me", async (req, res) => {
    var validUser = req.cookies["FOKUS_ID"] && req.cookies["FOKUS_TOKEN"]
    if (!validUser) return res.status(400).json({
        error: `Missing cookies '${[req.cookies["FOKUS_ID"] ? null : "FOKUS_ID", req.cookies["FOKUS_TOKEN"] ? null : "FOKUS_TOKEN"].filter(x => x != null).join(", ")}'.`,
        errorCode: "fokus.id.me.invalid_fields",
        statusCode: 400
    })

    if (tokens[req.cookies["FOKUS_ID"]] == req.cookies["FOKUS_TOKEN"]) {
        var user = await User.findOne({userid: req.cookies["FOKUS_ID"]}).lean()
        var tasks = await Tasks.findOne ({userid: req.cookies["FOKUS_ID"]}).lean()
        var subjects = await User.findOne({userid: req.cookies["FOKUS_ID"]}).lean()
        var calender = await Calender.findOne({userid: req.cookies["FOKUS_ID"]}).lean()

        res.json({
            userid: req.cookies["FOKUS_ID"],
            email: user.email,

            tasks: {
                tasks: tasks.tasks
            },
            subjects: {
                subjects: subjects.subjects
            },
            calender: {
                calender: calender.calender
            }

        })

    } else return res.status(401).json({
        error: `Invalid auth token '${req.cookies["FOKUS_TOKEN"]}'.`,
        errorCode: "fokus.id.me.invalid_auth_token",
        statusCode: 401

    })

app.get("/api/kill", (req, res) => {
    if (tokens[req.cookies["FOKUS_ID"]] == req.cookies["FOKUS_TOKEN"]) delete tokens[req.cookies["FOKUS_ID"]]
    res.clearCookie("FOKUS_ID")
    res.clearCookie("FOKUS_TOKEN")

    if (req.query.redirect) res.redirect("/login")
    else res.status(204).end()
})

app.post("/api/update", async (req, res) => {

    var validUser = req.cookies["FOKUS_ID"] && req.cookies["FOKUS_TOKEN"]

    if (!validUser) return res.status(400).json({
        error: `Missing cookies '${[req.cookies["FOKUS_ID"] ? null : "FOKUS_ID", req.cookies["FOKUS_TOKEN"] ? null : "FOKUS_TOKEN"].filter(x => x != null).join(", ")}'.`,
        errorCode: "fokus.id.update.invalid_fields",
        statusCode: 400
    })
    if (tokens[req.cookies["FOKUS_ID"]] != req.cookies["FOKUS_TOKEN"]) return res.status(401).json({
        error: `Invalid auth token '${req.cookies["FOKUS_TOKEN"]}'.`,
        errorCode: "fokus.id.me.invalid_auth_token",
        statusCode: 401
    })

    var pattern = new RegExp('^[0-9]+$')

    var updated = {}

    if (req.body.addtask) {
            var user = await Tasks.findOne({userid: req.cookies["FOKUS_ID"]}).lean()
            user.updateOne({
                $push: {
                    tasks: {
                        id: user.tasks.length,
                        name: req.body.name,
                        notes: req.body.description,
                        dueDate: req.body.dueDate,
                        subject: req.body.subject,
                        priority: req.body.priority,
                        status: req.body.status
                    }
                }
            })
            updated["addtask"] = "yes"
        }
    })

    if (req.body.modtask) {
        var user = await Tasks.findOne({userid: req.cookies["FOKUS_ID"]}).lean()
        user.updateOne({
            $set: {
                "tasks.$[i].name": req.body.name,
                "tasks.$[i].notes": req.body.description,
                "tasks.$[i].dueDate": req.body.dueDate,
                "tasks.$[i].subject": req.body.subject,
                "tasks.$[i].priority": req.body.priority,
                "tasks.$[i].status": req.body.status
            },
            arrayFilters: [{ "i.id": req.params.id }]
        })
    }

    if (req.body.addEvent) {
        var user = await Calender.findOne({userid: req.cookies["FOKUS_ID"]}).lean()
        user.addOne({
            $push: {
                calender: {
                    id: user.calender.length,
                    name: req.body.name,
                    notes: req.body.description,
                    dueDate: req.body.dueDate,
                    subject: req.body.subject,
                    priority: req.body.priority,
                    status: req.body.status
                }
            }
        })
    }

    if (req.body.modEvent) {
        var user = await Calender.findOne({userid: req.cookies["FOKUS_ID"]}).lean()
        user.updateOne({
            $set: {
                "calender.$[i].name": req.body.name,
                "calender.$[i].notes": req.body.description,
                "calender.$[i].dueDate": req.body.dueDate,
                "calender.$[i].subject": req.body.subject,
                "calender.$[i].priority": req.body.priority,
                "calender.$[i].status": req.body.status
            },
            arrayFilters: [{ "i.id": req.params.id }]
        })
    }

    if (req.body.addSubject) {
        var user = await Subjects.findOne({userid: req.cookies["FOKUS_ID"]}).lean()
        user.addOne({
            $push: {
                subjects: {
                    id: user.subjects.length,
                    name: req.body.name,
                    color: req.body.color,
                    schedule: req.body.schedule
                }
            }
        })
    }

    if (req.body.modSubject) {
        var user = await Subjects.findOne({userid: req.cookies["FOKUS_ID"]}).lean()
        user.updateOne({
            $set: {
                "subjects.$[i].name": req.body.name,
                "subjects.$[i].color": req.body.color,
                "subjects.$[i].schedule": req.body.schedule
            },
            arrayFilters: [{ "i.id": req.params.id }]
        })
    }

    res.json({
        updated: true,
        statusCode: 200
    })
})

app.get("/api/read", async (req, res) => {
    if (!req.cookies["FOKUS_ID"]) return res.status(401).json({
        error: "No auth token provided.",
        errorCode: "fokus.id.me.no_auth_token",
        statusCode: 401
    })
    if (tokens[req.cookies["FOKUS_ID"]] != req.cookies["FOKUS_TOKEN"]) return res.status(401).json({
        error: `Invalid auth token '${req.cookies["FOKUS_TOKEN"]}'.`,
        errorCode: "fokus.id.me.invalid_auth_token",
        statusCode: 401
    })

    if (req.query.q == "tasks") {
        var user = await Tasks.findOne({userid: req.cookies["FOKUS_ID"]}).lean()
        res.json({
            tasks: user.tasks,
            statusCode: 200
        })
    }

    if (req.query.q == "event") {
        var user = await Calender.findOne({userid: req.cookies["FOKUS_ID"]}).lean()
        res.json({
            calender: user.calender,
            statusCode: 200
        })
    }

    if (req.query.q == "subject") {
        var user = await Subjects.findOne({userid: req.cookies["FOKUS_ID"]}).lean()
        res.json({
            subjects: user.subjects,
            statusCode: 200
        })
    }
})

module.exports = app
