const mongoose = require("mongoose")

const schema = new mongoose.Schema({
    userid: {
        type: String,
        required: true
    },
    events: {
        type: Array,
        required: true,
        default: []
    }
})

module.exports = mongoose.model("calender", schema)