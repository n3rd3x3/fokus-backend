const mongoose = require("mongoose")

const schema = new mongoose.Schema({
    userid: {
        type: String,
        required: true
    },
    tasks: {
        type: Array,
        default: []
    }
})

module.exports = mongoose.model("tasks", schema)