const mongoose = require("mongoose")

const schema = new mongoose.Schema({
    userid: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean,
        required: false,
        default: false
    },
    subscription: {
        type: Boolean,
        required: true,
        default: false
    }

})

module.exports = mongoose.model("user", schema)